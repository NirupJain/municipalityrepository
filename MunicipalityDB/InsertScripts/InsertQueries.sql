USE [MunicipalityDB]

SET IDENTITY_INSERT [dbo].[TaxPeriod] ON
INSERT INTO [dbo].[TaxPeriod] ([PeriodId], [Period]) VALUES (1, N'Yearly')
INSERT INTO [dbo].[TaxPeriod] ([PeriodId], [Period]) VALUES (2, N'Monthly')
INSERT INTO [dbo].[TaxPeriod] ([PeriodId], [Period]) VALUES (3, N'Weekly')
INSERT INTO [dbo].[TaxPeriod] ([PeriodId], [Period]) VALUES (4, N'Daily')
SET IDENTITY_INSERT [dbo].[TaxPeriod] OFF

SET IDENTITY_INSERT [dbo].[MunicipalityTaxes] ON
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (1, N'Copenhagen', N'2020-07-22 00:00:00', N'2020-07-22 00:00:00', 4, CAST(0.66 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (2, N'Copenhagen', N'2020-07-01 00:00:00', N'2020-07-31 00:00:00', 2, CAST(0.30 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (3, N'Copenhagen', N'2020-01-01 00:00:00', N'2020-12-31 00:00:00', 1, CAST(0.30 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (4, N'Odense', N'2020-05-22 00:00:00', N'2020-05-22 00:00:00', 4, CAST(0.33 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (5, N'Odense', N'2020-05-01 00:00:00', N'2020-05-31 00:00:00', 2, CAST(0.66 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (6, N'Odense', N'2020-05-24 00:00:00', N'2020-05-24 00:00:00', 4, CAST(0.34 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (7, N'Odense', N'2019-01-01 00:00:00', N'2020-12-31 00:00:00', 1, CAST(0.11 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (8, N'Horsens', N'2019-01-01 00:00:00', N'2019-01-01 00:00:00', 4, CAST(1.00 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (9, N'Horsens', N'2019-01-04 00:00:00', N'2019-01-04 00:00:00', 1, CAST(0.99 AS Decimal(18, 2)))
INSERT INTO [dbo].[MunicipalityTaxes] ([Id], [Municipality], [FromDate], [ToDate], [PeriodId], [TaxApplied]) VALUES (12, N'Aalborg', N'2020-02-01 00:00:00', N'2020-02-01 00:00:00', 4, CAST(2.33 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[MunicipalityTaxes] OFF