﻿CREATE TABLE [dbo].[MunicipalityTaxes] (
    [Id]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [Municipality] VARCHAR (500)   NULL,
    [FromDate]     DATETIME        NULL,
    [ToDate]       DATETIME        NULL,
    [PeriodId]     BIGINT          NULL,
    [TaxApplied]   DECIMAL (18, 2) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

