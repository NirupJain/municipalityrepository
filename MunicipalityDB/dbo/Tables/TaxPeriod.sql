﻿CREATE TABLE [dbo].[TaxPeriod] (
    [PeriodId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [Period]   VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([PeriodId] ASC)
);

