﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MunicipalityAPI.Models;
using Swashbuckle.AspNetCore.Annotations;


namespace MunicipalityAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MunicipalityController : ControllerBase
    {
        private readonly MunicipalityDBContext _context;

        public MunicipalityController(MunicipalityDBContext context)
        {
            _context = context;
        }

        // GET: api/Municipality
        [HttpGet]
        [SwaggerOperation(Summary = "Get Municipality Information", Description = "Get Municipality Information")]
        [SwaggerResponse(200)]
        [Produces("application/json")]
      
        public async Task<ActionResult<IEnumerable<MunicipalityTaxes>>> GetMunicipalityTaxes()
        {
            return await _context.MunicipalityTaxes.OrderByDescending(c=>c.PeriodId).ToListAsync();
        }

        // GET: api/Municipality/5
        [HttpGet("{municipality}")]
        [SwaggerOperation(Summary = "Get Municipality tax for mentioned date", Description = "Get Municipality tax for mentioned date")]
        [ActionName("GetMunicipalityTaxes")]
        public async Task<ActionResult<MunicipalityTaxes>> GetMunicipalityTaxes(string municipality,DateTime date)
        {
            var municipalityTaxes = await _context.MunicipalityTaxes
                                    .Where(c=>c.Municipality == municipality && (c.FromDate<=date.Date && c.ToDate>=date.Date))
                                    .OrderByDescending(c => c.PeriodId)
                                    .FirstOrDefaultAsync();

            if (municipalityTaxes == null)
            {
                return NotFound();
            }

            return municipalityTaxes;
        }


        // PUT: api/Municipality/5
        [ActionName("PutMunicipalityTaxes")]
        [SwaggerOperation(Summary = "Update Municipality tax for mentioned id", Description = "Update Municipality tax for mentioned id")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMunicipalityTaxes(long id, MunicipalityTaxes municipalityTaxes)
        {
            if (id != municipalityTaxes.Id)
            {
                return BadRequest();
            }

            _context.Entry(municipalityTaxes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MunicipalityTaxesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // POST: api/Municipality
        [ActionName("PostMunicipalityTaxes")]
        [SwaggerOperation(Summary = "Insert new Municipality data", Description = "Insert new Municipality data")]
        [HttpPost]
        public async Task<ActionResult<MunicipalityTaxes>> PostMunicipalityTaxes(MunicipalityTaxes municipalityTaxes)
        {
            municipalityTaxes.FromDate = municipalityTaxes.FromDate.Value.Date;
            municipalityTaxes.ToDate = municipalityTaxes.ToDate.Value.Date;
            
            _context.MunicipalityTaxes.Add(municipalityTaxes);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MunicipalityTaxesExists(municipalityTaxes.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMunicipalityTaxes", new { id = municipalityTaxes.Id }, municipalityTaxes);
        }


        // POST: api/Municipality
        [ActionName("PostMunicipalityTaxesFromCSV")]
        [SwaggerOperation(Summary = "Insert new Municipality data from .csv file", Description = "Insert new Municipality data from .csv file")]
        [HttpPost("sourcePath")]
        public async Task<ActionResult<MunicipalityTaxes>> PostMunicipalityTaxesFromCSV(string sourcePath)
        {

            using (StreamReader reader = new StreamReader(sourcePath, Encoding.UTF8))
            {


                var csvReader = new CsvReader(reader, System.Globalization.CultureInfo.CurrentCulture);
                var records = csvReader.GetRecords<MunicipalityTaxes>();

                try
                {
                    await _context.BulkInsertAsync<MunicipalityTaxes>(records.ToList());
                }
                catch (DbUpdateException)
                {
                    throw;

                }
            }
            return NoContent();
        }


        // DELETE: api/Municipality/5
        [ActionName("DeleteMunicipalityTaxes")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<MunicipalityTaxes>> DeleteMunicipalityTaxes(long id)
        {
            var municipalityTaxes = await _context.MunicipalityTaxes.FindAsync(id);
            if (municipalityTaxes == null)
            {
                return NotFound();
            }

            _context.MunicipalityTaxes.Remove(municipalityTaxes);
            await _context.SaveChangesAsync();

            return municipalityTaxes;
        }

        private bool MunicipalityTaxesExists(long id)
        {
            return _context.MunicipalityTaxes.Any(e => e.Id == id);
        }
    }
}
