﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MunicipalityAPI.Models
{
    public partial class MunicipalityDBContext : DbContext
    {
        public MunicipalityDBContext()
        {
        }

        public MunicipalityDBContext(DbContextOptions<MunicipalityDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MunicipalityTaxes> MunicipalityTaxes { get; set; }
        public virtual DbSet<TaxPeriod> TaxPeriod { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=MunicipalityDB;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MunicipalityTaxes>(entity =>
            {
                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.Municipality)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TaxApplied).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ToDate).HasColumnType("datetime");

                
            });

            modelBuilder.Entity<TaxPeriod>(entity =>
            {
                entity.HasKey(e => e.PeriodId)
                    .HasName("PK__TaxPerio__E521BB166CAF0C19");

                entity.Property(e => e.Period)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
