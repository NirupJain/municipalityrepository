﻿using System;
using System.Collections.Generic;

namespace MunicipalityAPI.Models
{
    public partial class MunicipalityTaxes
    {
        public long Id { get; set; }
        public string Municipality { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? PeriodId { get; set; }
        public decimal? TaxApplied { get; set; }

    }
}
