﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MunicipalityAPI.Models
{
    public partial class TaxPeriod
    {
        
        public long PeriodId { get; set; }
        public string Period { get; set; }

    }
}
